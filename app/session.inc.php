<?php

error_reporting(0);
ini_set('display_errors', 0);

if (!defined("IS_MAIN")) {
    die();
}

session_start();

if(!isset($_SESSION['folder'])) {
    $folder = '/tmp/sess/' . sha1(session_id()) . '/';
    if (mkdir($folder) === false) {
        $folder = '/tmp/sess/' . sha1(session_id()) . '/';
        mkdir($folder);
    }
    $_SESSION['folder'] = $folder;
}

