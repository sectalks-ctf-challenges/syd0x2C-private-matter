<?php

error_reporting(0);
ini_set('display_errors', 0);

const IS_MAIN = true;
require_once(__DIR__ . '/common.inc.php');
require_once(__DIR__ . '/session.inc.php');
require_once(__DIR__ . '/crypto.inc.php');

if (!isset($_POST)) {
    die();
}

global $flag;

$data = null;
$in = file_get_contents('php://input');

if (strlen($in) > 0) {
    $data = json_decode($in, true);
    $data = str_ireplace(',', '_', $data);

    if (isset($data['status']) && strpos($data['status'], 'eventStart') > -1) {
        $data['timestamp'] = filter_var($data['timestamp'], FILTER_SANITIZE_NUMBER_INT);
        $fname = $_SESSION['folder'] . $data['timestamp'] . '_' . $ANALYTICS_FILE_NAME;
        $f = fopen($fname, 'wb');
        session_write_close();

        $newdata = Array(
            $data['type'], // needs encryption for GDPR
            $data['url'], // needs encryption for GDPR (to prevent user tracking)
            $data['ua'], // needs encryption for GDPR (very important PII)
            $data['timestamp'], // not important for GDPR but we'll still encrypt it for privacy, who knows those hackers...
            $data['status'], // needs encryption for GDPR
            $data['event_data']['type'], // needs encryption for GDPR
            $data['event_data']['target'], // needs encryption for GDPR
            $data['event_data']['x'], // (x, y) coordinates need encryption for GDPR (can't leak the location of clicks)
            $data['event_data']['y']  // (x, y) coordinates need encryption for GDPR (can't leak the location of clicks)
        );

        // cache the file not to loose it if encryption fails
        fwrite($f, json_encode($newdata));
        fclose($f);

        // hash the key (flag) as many times as we have data
        // (the more data, the more we need to hash for security)
        // bcrypt with many rounds is very secure
        // we can always validate that metrics are intact and have not been forged by simply checking the hash later
        $opts = Array('cost'=>(sizeof($data) > 4 ? sizeof($data) : 4));

        $key = password_hash($flag, PASSWORD_BCRYPT, $opts);
        // var_dump(sizeof($data)); // to ensure there's enough hash rounds

        // use our new key to encrypt the analytics data for GDPR
        $encrypted_data = Array(
            encrypt($data['type'], $key),
            encrypt($data['url'], $key),
            encrypt($data['ua'], $key),
            encrypt($data['timestamp'], $key),
            encrypt($data['status'], $key),
            encrypt($data['event_data']['type'], $key),
            encrypt($data['event_data']['target'], $key),
            encrypt($data['event_data']['x'], $key),
            encrypt($data['event_data']['y'], $key)
        );

        $final = json_encode(
            Array(
                "key" => $key,
                "data" => json_encode($encrypted_data),
            )
        );

        // save the encrypted data
        $f = fopen($fname, 'wb');
        fwrite($f, $final);
        fclose($f);

        header("X-Analytics: " . substr($key, 7, strlen($key)), false, 200);
    }
} else {
    die_nicely();
}

