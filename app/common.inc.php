<?php

error_reporting(0);
ini_set('display_errors', 0);

if (version_compare('7.0.0', PHP_VERSION) >= 0) {
    // C'mon really, move off of version 5 already, version 7 is way more secure
    die("This site requires PHP 7 and up");
}

if (!defined("IS_MAIN")) {
    die();
}

const SITE_TITLE = "Laulls&Co";
//$GIT_RELEASE_VERSION = exec("git rev-parse HEAD");
$GIT_RELEASE_VERSION = shell_exec("git log HEAD --pretty");
$ANALYTICS_FILE_NAME = 'analytics.json';
$flag = file_get_contents('/flag.txt');

function die_nicely($s='Internal Server Error') {
    header("Error:" . $s, true, 500);
    die();
}

