<?php

error_reporting(0);
ini_set('display_errors', 0);

if (!defined("IS_MAIN")) {
    die();
}

function encrypt($data, $key) {
    $ivlen = openssl_cipher_iv_length("aes-128-gcm");
    $iv = openssl_random_pseudo_bytes($ivlen);

    if (version_compare(PHP_VERSION, '7.2.0') >= 0) {
        // damn PHP 7.2 requires $tag
        $tag = null;
        return openssl_encrypt($data, "aes-128-gcm", $key, $options=0, $iv, $tag);
    }
    else {
        return openssl_encrypt($data, "aes-128-gcm", $key, $options = 0, $iv);
    }
}
