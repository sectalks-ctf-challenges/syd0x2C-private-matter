<?php

error_reporting(0);
ini_set('display_errors', 0);

const IS_MAIN = true;

require_once(__DIR__ . '/common.inc.php');
require_once(__DIR__ . '/session.inc.php');

if (isset($_GET['page'])) {
    $page = $_GET['page'];
    // Prevent arbitrary file inclusion and path traversal
    $page = preg_replace('(\.\.|:|git||flag|txt|docker|sock|file|data|phar|zip|gopher|php|phtml|http|https|zlib|convert|compress|expect|ftp|glob|ssh2|rar|ogg|bin|boot|cdrom|dev|etc|home|root|lib|lib32|lib64|libx32|media|mnt|opt|proc|var|run|sbin|snap|srv|sys|usr|vmlinuz|initrd)', '', $page);
    require_once($page);
} else {
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head><meta name='robots' content='noindex, nofollow'>
		<title><?=SITE_TITLE?></title>
		<!-- META -->
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, maximum-scale=1"/> 
	
	
	
	
		<script src="https://www.youtube.com/iframe_api"></script>
		<!-- CSS -->
		<link rel="stylesheet" href="//www.imcreator.com/css/common.css?v=1.4.8d4">
		<style>
			@font-face {
				font-family: 'HelveNueThinNormal';
				src: url('https://storage.googleapis.com/xprs_resources/fonts/helveticaneuethn-webfont.eot');
				src: url('https://storage.googleapis.com/xprs_resources/fonts/helveticaneuethn-webfont.eot?iefix') format('eot'),
					url('https://storage.googleapis.com/xprs_resources/fonts/helveticaneuethn-webfont.woff') format('woff'),
					url('https://storage.googleapis.com/xprs_resources/fonts/helveticaneuethn-webfont.ttf') format('truetype'),
					url('https://storage.googleapis.com/xprs_resources/fonts/helveticaneuethn-webfont.svg#webfontJR0GaG5Z')
					format('svg');
				font-weight: normal;
				font-style: normal;
			}
		</style>
		<link rel="stylesheet" type="text/css" href="//www.imcreator.com/all_css.css?v=1.4.8d4" />

	
		
			<link rel="icon" type="image/png" href="https://lh3.googleusercontent.com/dd_e0xA19up9208Tv6odcjHEw6z4cKAA6fTgjZ9ynkKoSnr5R4vFxI7gZp6pnZH1Vi5T5f-fphjFsrLG=s30" />
			<link rel="apple-touch-icon" href="https://lh3.googleusercontent.com/dd_e0xA19up9208Tv6odcjHEw6z4cKAA6fTgjZ9ynkKoSnr5R4vFxI7gZp6pnZH1Vi5T5f-fphjFsrLG=s30" />
		
	
	
		<script src="https://imos006-dot-im--os.appspot.com/js/imos.js?v=1.4.8d4" type="text/javascript"></script>
		<script type="text/javascript">IMOS.pageView();</script>
		
	
		
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	
	<link id="vbid-27694307-6jjfjyp2-STRIPE_DATA" rel="stylesheet" type="text/css"  href="//www.imcreator.com/stripe_data_css?page_id=vbid-27694307-6jjfjyp2-STRIPE_DATA&v=1.4.8d4"> 
	
	<link rel="stylesheet" type="text/css" href="//www.imcreator.com/css/fonts.css?v=1.4.8d4" />
		<link rel="stylesheet" type="text/css" href="//www.imcreator.com/css/effects.css?v=1.4.8d4" />
		<link rel="stylesheet" type="text/css" href="//www.imcreator.com/css/lightbox.css?v=1.4.8d4">
	
	<link rel="stylesheet" type="text/css" href="//www.imcreator.com/css/previewhelper.css?v=1.4.8d4">
	
	<link rel="stylesheet" type="text/css" href="//www.imcreator.com/css/spimeview.css?v=1.4.8d4">
	
	<link id="vbid-27694307-6jjfjyp2-STATIC_STYLE" rel="stylesheet" type="text/css" href="//www.imcreator.com/static_style?v=1.4.8d4&vbid=vbid-27694307-6jjfjyp2&caller=static">
	
		
		<!-- SCRIPT -->
		<script src="https://code.jquery.com/jquery-2.x-git.min.js" type="text/javascript"></script>
		<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCsEwUCqbjiMuQWXV875E4ks3J6tZLAnDU&libraries=places' type='text/javascript'></script>
		
		<script src="//www.imcreator.com/js/xprs_helper.js?v=1.4.8d4" type="text/javascript"></script>
		

		
		<script type="text/javascript" src="//www.imcreator.com/all_js.js?v=1.4.8d4"></script>
		<script src="//www.imcreator.com/js/lib/touchswipe/jquery.mobile.custom.min.js"></script>
		
		
	
	
		<script src="//www.imcreator.com/js/preview_helper.js?v=1.4.8d4"  type="text/javascript"></script>
	
	
	<!-- User analytics  -->


	

	<!-- Label config-->
		<script>
		
		 var LABEL_CONFIG = {};
		 
		</script></head>

	<body class="fast-animated-bg fixed-bg  " data-ecommerce-solution="DISABLED" data-root-id="vbid-27694307-6jjfjyp2" data-root-style-id="style-27694307-clgaknqg" data-default-currency="USD" data-osid="osid--2c4f526f-2f8f5dbf"  data-app-version="1.4.8d4" data-caller="static"  data-ecommerce-dashboard="" data-static-server="//www.imcreator.com" data-imos-server="https://imos006-dot-im--os.appspot.com" >
	
		<div id="xprs" data-website-name="laullsandco" data-slug="" class="xprs-holder" >
			<div class="main-page "   >
				<div id="content">
					<div  id="vbid-27694307-6jjfjyp2"  class="master container style-27694307-clgaknqg content stripes  website-style " data-itemtype="folder" data-creator="" data-itemname="laullsandco"  data-itemslug="smithco-law-firm" data-itemstyleid="style-27694307-clgaknqg" data-margintop="" data-arranger="stripes" data-layout="multi" data-vbid="vbid-27694307-6jjfjyp2" data-preset-type-id="" data-preview-style="style-27694307-clgaknqg" data-style="style-7f715-gkcpu3e69m" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="stripes"
		data-ARRANGER_COLS="0"
		data-ARRANGER_ITEM_MAX_WIDTH="400"
		data-ARRANGER_ITEM_MIN_WIDTH="230"
		data-ARRANGER_ITEM_RATIO="1"
		data-ARRANGER_ITEM_SPACING="20"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="false"
		data-AUTO_PLAY_DURATION="4"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT=""
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
	
	
	
		<div  id="vbid-27694307-hqtchh7a"  class="master item-box  element-box style-27694307-clgaknqg          " data-holder-type="element"" data-holder-type="element"  data-child-type="RAW"  data-styleid="style-27694307-clgaknqg"  data-preset-type-id="">
			<div id="" class="stripe-background load-high-res " ></div>
			<div class="element-wrapper item-wrapper raw-wrapper ">
					
						<div class="content">
					
					<div class="page-raw-cover dynamic-height " >
	<div id="vbid-27694307-hqtchh7a" class="element html-source magic-circle-holder page_stripe_popup_app page-app" data-menu-name="RAW" data-json-name="HTML_IFRAME" data-allow-script="True" >
	<div class="raw-container" id="vbid-27694307-hqtchh7a-raw" data-raw-content-url="/html_src/ba1a34bd?raw=true&amp;type=page_stripe_popup_app" data-static="true" >
	
		
		
 <!-- 
 <select class='app_field_input' style="width:150px;" name="template" onChange="templateSelected(this.value)">
	  <option value="coupon">Coupon</option>
	  <option value="promotion">Promotion</option>
	  <option value="subscribe">Subscribe</option>
	  <option value="contact">Contact</option>
	  <option value="feedback">Feedback</option>
 </select>
-->	

<div id="app_settings" class="popup-form-settings-panel" style="display:none;height:95%;line-height:1em">
 
	 <div id="template_selector">
	 	 <div id="selectTemplateButton" class="template-blank" onClick="expandPopupList()"><span style="color: white;margin-left: 16px;">Pop-up Preset:</span></div>
		 <ol id="selectable" name="template" value="vbid-3726659f-w2ypdu1q"> 
			<li class="ui-widget-content item" value="vbid-3726659f-w2ypdu1q" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/jMZ2Tw7t9eo82rnDlzvQDDSZNwIiAbqay7BsrRp52KG-pG-z1FgVaJhM98fGZG1ccpaOBxBMKyhzXZcu=s250')"></li>
			<li class="ui-widget-content item" value="vbid-a89f3468-iidd4ogs" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/VHK8JHwEKjVw5TrU4PFrbz8hPl9gcYyBBks4sKi-Fu4E49EkyJP5kkKIwyGza_twYzEzz9cWYj4bZZXHlg=s250')"></li>
			<li class="ui-widget-content item" value="vbid-09eff374-gv8ydiax" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/DWJSGNQnK3rUlrSrNHh42jPK5Vbv3BADTVKT6rTCdgvTUMdDOwRl9zqdj7pGGvWEv5CYsBccfGLPvW7vcg=s250')"></li>
			<li class="ui-widget-content item" value="vbid-68e23454-gq2iu6ee" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/JHTgLyt3TylCweRRUgmDOj2GYgUQSe5PG-ouOGhc0Tbd5E8gTZRlgZ6DD3rjIrO5_uJuExOIoc-ikbFl0QI=s250')"></li>
			<li class="ui-widget-content item" value="vbid-d0f47672-abrgqdbx" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/A1q1oUvOhHr5x6XPCg25ZSVWlHZiGYNyJjg3KEo0AQkW7WZ8-T97NxHlERgE-HHBOtrkr5WviuD3VczAHw=s250')"></li>
			<li class="ui-widget-content item" value="vbid-62eb84cb-2qvkpfd1" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/lI9-tyIeV4shBhB3CCtPUlLJrCcFfA728fRiwTZUGog9IPqnYf-hkFrpcQJBG2txCyo7qRa-9jLWRm119Yg=s250')"></li>
			<li class="ui-widget-content item" value="vbid-14be725f-pzignpuh" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/pxUXT18hfh3c5fxXuNup8HOAX321dtizmIyBuBuk7ZLEGTMuoIwoRN2dSaMGpJTxMj0BLu_qD0eBfDBp=s250')"></li>
			<li class="ui-widget-content item" value="vbid-a7d541ad-2iozfb83" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/Uwc3LraWb-syaYpszX2GgAJCLhd7EpCzeZ9s-wdv40oCELZuki2LO67NCEqQ4eOX3jq97sBzhwaJJOfNkTQ=s250')"></li>
			<li class="ui-widget-content item" value="vbid-e237c3c6-ayispqnk" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/4ggYTwR9MatkGWanFU1nQTwp9JrRWYf8zBgG0dOpgQiwPIk4tYtqvgL_4DwTKdedHOU_NKShA7Zde_nsbCU=s250')"></li>
			<li class="ui-widget-content item" value="vbid-7223cae6-2qvkpfd1" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/_mkGXVIE9zotGkYvdJzqu8O3w6Ni7vAHIulYFpYKIz8aoYEG1nGG2za31ijw9w46yclRz6-2Y6-fuAK_pQ=s250')"></li>
			<li class="ui-widget-content item" value="vbid-02ce650d-0e85yjxy" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/T_yIk-VmAwIwZ5rL6r8_zH8IyX6qSxCSERuGwwwDkNw0WE9ZemxE3f5jXo3mmWXOZFzDFqdHB5g1qrmTSAs=s250')"></li>
			<li class="ui-widget-content item" value="vbid-bddafde3-0xlbdgxw" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/sQVik7kseAfLUnZwEHCspRYPCB8kCqATmSrrncLccDpTvZZhGyP2HzHyCVWCMDvyU8mee12aZFWXm8B_=s250')"></li>
			<li class="ui-widget-content item" value="vbid-ad1b7e75-udmmq7ka" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/zohwE8liYP0dVl96s3CsOR-26tDzcCI3A3F4X35DbKyGxKssTh0UyviLBSNTBpfCgK4JEbhCeDcDEBNNXw=s250')"></li>
			<li class="ui-widget-content item" value="vbid-a86ce9b6-2qvkpfd1" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/DOnLWB4PeczAQPeWWpq9voKMb1mVkgRpWpxm7uX_yjoLagAxzIJFauWVHQqqzW4bFaD0mKhWowYPDDHldA=s250')"></li>
			<li class="ui-widget-content item" value="vbid-b7756e4f-gq2iu6ee" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/WCR2bG62NvuH6t9gN280oemGW8JYIZnVt_UFC3wM3ZRCdkl5Po619_Tfgb9pglMGPiFwQhwgIxdoqDlS=s250')"></li>
			<li class="ui-widget-content item" value="vbid-57286964-gq2iu6ee" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/P1eBKYoYrGaj3ZV7swtXte706n4jfuIqTRvuNDTljpOZUBhgArr3uD0yZSoJWe7cqM19fEfELNC0DVFJkQ=s250')"></li>
			<li class="ui-widget-content item" value="vbid-627dc7e6-s5pdf8f2" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/rrrM4d3RIw5wYJ1TAo0gt0G09mwJ5sXLvAQt8zTBl8aCs_BJXP4zH_lvfVE3y-J-zek5-i9NmR-mv8zW=s250')"></li>
			<li class="ui-widget-content item" value="vbid-8ff732dd-abrgqdbx" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/tBT3-1uAyuGx4N7g9L9izG6TZoVf96Ly-9OgvTBJaTRqLWGOiIizej9ekbjKnyD7BmaAcJcw1WYHap_M=s250')"></li>
			<li class="ui-widget-content item" value="vbid-466845d0-iidd4ogs" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/ApteRE0sFclf1R2YJCTxGfXtgTdkKaTqj0KpFD0vwfmOxQyhMKGlUqFKm5pzcor1yd99aM72ygnUxVAVVg=s250')"></li>
			<li class="ui-widget-content item" value="vbid-dd9c4df1-u8eh7xv3" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/9xxMvka-ASkV2HFYfqn09hrBvTnhpdSt8tw53ULB19ZXhhSR0DNGCc4ON4zrvXGiin2U309HL-Psqa_m=s250')"></li>
			<li class="ui-widget-content item" value="vbid-e59d5107-kqgmxt5n" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/7s2eqiLGtVW2cKq9PmypBj7DPgK1QTI1M0TqKCyIjpk1Wm8uTjGgUs2vV6glSoA90MBYOCGWxW0vQ9Ri-S4=s250')"></li>
			<li class="ui-widget-content item" value="vbid-05fbf368-36hyxloh" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/j2UVF0-_NqZitrzY7Jw_dB8NwbKR_Oe6jt8nV8w2PG3v4N-MSxqrJiAnmBBrxSl88vXEuuOr44pZFjT_=s250')"></li>
			<li class="ui-widget-content item" value="vbid-71c36603-wgdrzlzs" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/MHyRWCeEK8VShirpKgHhNHaEE5xWhpEYJwUOU493peeHFU_140TE0QYHOc3SYxFXmTXbslSda5Kor5cyMQ=s250')"></li>
			<li class="ui-widget-content item" value="vbid-275d0285-tpvaafiz" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/gf1RbaxV8A9eXPzZm1xdosCEBJpTtwRs09VeEezhMlz86t0t1iNpXyzOhld_2-btPD8pnTBjlkAfzaM5kQ=s250')"></li>
			<li class="ui-widget-content item" value="vbid-ae924c98-yeqviu8q" style="background-position: center;background-image:url('https://lh3.googleusercontent.com/uD8xDAgVKM8wavlw9JwcE1UNMKU-9rSaZ8HlRayRNRw90JClCJRo3_3Cj-zez000qwXpNqA2DXm-KttA2KY=s250')"></li>
		</ol>
	</div>
	<div id='template_params' style="padding: 15px;">
		 <label class='app_text_label' style="width:103px;margin-left: -8px;">Email for replies: </label>
		 <input class='app_field_input ' type="text" name="data_href" style="width: 265px;" placeholder="your@email.com"><br>
		 <textarea class='app_field_input ' style="width: 265px; height:50px" type="text" rows="3" name="data_text"  placeholder="Text to be presented after form submission"></textarea> 
		
		 <label class='app_text_label' style="width:103px;margin-left: -8px;">Location: </label>
		  <select class='app_field_input' style="width:150px" name="locationOption" onChange="locationSelected(this.value)">
			  <option value="center">Centered</option>
			  <option value="bottom_right">Bottom Right</option>
			  <option value="fullscreen">Fullscreen</option>
		 </select>
		 <br>
		  
		 <label class='app_text_label short_label' name="popupWidth">Width </label>
		 <input class='app_field_input short_field' type="number" name="popupWidth" style="width:70px">
		 
		 <label class='app_text_label short_label' name="popupHeight" style="margin-left: 35px;">Height </label>
		 <input class='app_field_input short_field' type="number" name="popupHeight" style="width:70px"><br><br>
		
		 <label class='app_text_label' style="width:8em;margin-left: -8px;">Pop Options: </label><br>
		 <input class='app_field_input' type="checkbox" id="timer" name="popingOptions" checked><label class='app_text_label' style="margin-left: 6px;margin-right: 0px;margin-top: 5px;">Timer </label>
		 <input class='app_field_input' type="checkbox" id="on_scroll" name="popingOptions"><label class='app_text_label' style="margin-left: 6px">On Scroll </label>
		 <input class='app_field_input' type="checkbox" id="on_exit" name="popingOptions" style="margin-left: 6px;"><label class='app_text_label' style="margin-left: 6px;margin-right: 0px;">On Exit </label>
		 <br>
		
		 <div class="extra_options timer" style="display:none">
		 	<label class='app_text_label long_label' style='margin-right: 5px;'>Pop after</label>
		 	<input class='app_field_input short_field' type="number" name="timeToPopup">
		 	<label class='app_text_label long_label' style='width:67px;margin-left: 5px;'>  seconds</label><br>
		 </div>
		
		 <div class="extra_options on_scroll" style="display:none">
		 	<label class='app_text_label long_label' style='margin-right: 5px;'>Pop after scrolling</label>
		 	<input class='app_field_input short_field' type="number" name="endPrecent"> 
		 	<label class='app_text_label long_label' style='width: 77px;margin-left: 5px;'> % of the page</label><br>
		 </div>
		 
		 <!-- 
		 <div class="extra_options on_exit" style="display:none">
		 	<label class='app_text_label long_label' style="margin-top: 5px;">Every page Reload </label>
		 	<input class='app_field_input' type="checkbox" name="aggressive"><br>
		 </div>
		  -->
	</div>
</div>

<script>

var/*<{*/popupWidth/*}*/ = /*{*/400/*}>*/;
var/*<{*/popupHeight/*}*/ = /*{*/600/*}>*/;
var /*<{*/timeToPopup/*}*/ = /*{*/5/*}>*/; //in seconds
var /*<{*/endPrecent/*}*/ = /*{*/80/*}>*/;
var /*<{*/popingOptions/*}*/ = /*{*/[]/*}>*/;
var /*<{*/locationOption/*}*/ = /*{*/"bottom_right"/*}>*/;
var /*<{*/template/*}*/ = /*{*/"vbid-3726659f-w2ypdu1q"/*}>*/;
var /*<{*/data_href/*}*/ = /*{*/""/*}>*/;
var /*<{*/data_text/*}*/ = /*{*/""/*}>*/;

var stripeObj;
var backgroundObj;


if (typeof EditorHelper == "undefined"){
	if (popingOptions.indexOf("timer") != -1) {
		document.addEventListener('DOMContentLoaded', setTimeout(popUpStripe, timeToPopup*1000));
	}
} else {
	document.addEventListener('xprs-app-loaded', popUpStripe);
	document.addEventListener('xprs-app-loaded', extraAppOptions);
	//console.log('listeners added');
}

function locationSelected(selVal) {
	if (selVal == 'fullscreen') {
		
		popupWidth=window.innerWidth;
		popupHeight=window.innerHeight;

		$("[name=popupWidth]").css('display','none');
		$("[name=popupHeight]").css('display','none');			

		$("[name=popupWidth]").val(popupWidth);
		$("[name=popupHeight]").val(popupHeight);
		
	} else if (selVal == 'bottom_right' || selVal == 'center') {
		
		if (selVal == 'center') {
			popupWidth=1200;
			popupHeight=600;
		}
		
		if (selVal == 'bottom_right') {
			popupWidth=400;
			popupHeight=600;
		}
		
		$("[name=popupWidth]").val(popupWidth);
		$("[name=popupHeight]").val(popupHeight);
		
		$("[name=popupWidth]").css('display','inline-block');
		$("[name=popupHeight]").css('display','inline-block');
	}
	
	$(".shown #update-html").trigger( "click" );
}

function fillPopupList() {
	/*
	var popupSiteVbid = 'vbid-stripe-popup-form-site';//'vbid-09eff374-npdbqrgi'
	
	XPRSHelper.GET("/get_box", {"vbid":popupSiteVbid,"no_style":true},function(jsonObj) {
		
		boxChildren = jsonObj['CHILDREN'];
		
		if (boxChildren){
            for idx, entry in enumerate(boxChildren):
                entryType = str(entry.keys()[0])
                if entryType == "CHILD":
                	if "TYPE" in entry["CHILD"] and entry["CHILD"]["TYPE"] == "FORM"
		
		console.log(jsonObj['CHILDREN']);
		}
	});
	*/
}


function templateSelected(selVal) {

	//var popupStripeVbid = 'vbid-stripe-popup-form-' + selVal;//+ $(this).prop('id');

	var displayedPanel = $('.shown #app_settings').find('#selectTemplateButton span');
	displayedPanel.addClass("loading-state");

	closeStripe();
	duplicateStripe(selVal, displayedPanel);
}

function extraAppOptions() {
	
	//extra options display
	if (popingOptions.indexOf("timer") != -1) {
		$('#app_settings .extra_options.timer').css('display','block');
	} else {
		$('#app_settings .extra_options.timer').css('display','none');
	}
	
	if (popingOptions.indexOf("on_exit") != -1) {
		$('#app_settings .extra_options.on_exit').css('display','block');
	} else {
		$('#app_settings .extra_options.on_exit').css('display','none');
	}
	
	if (popingOptions.indexOf("on_scroll") != -1) {
		$('#app_settings .extra_options.on_scroll').css('display','block');
	} else {
		$('#app_settings .extra_options.on_scroll').css('display','none');
	}
	
	
	$("input.app_field_input[type=checkbox]").unbind("click").bind("click",function(){
		
		var checkboxId = $(this).attr('id');

		if ($('#app_settings .extra_options.'+checkboxId).css('display') == 'none') {
			$('#app_settings .extra_options.'+checkboxId).css('display','block');
		} else {
			$('#app_settings .extra_options.'+checkboxId).css('display','none');
		}
	});
	
	var submitBtn = $(".stripe_popup_app a[data-link-type=SUBMIT]");
	
	var newText = $('.shown textarea[name=data_text]').val();
	var btnText = submitBtn.attr("data-text");
	
	var newEmail = $('.shown input[name=data_href]').val();
	var btnEmail = submitBtn.attr("data-href");

	
	if ((typeof newText != "undefined" && newText != "") && (typeof newEmail != "undefined" && newEmail != "")) {
	
		if (newText != btnText || newEmail != btnEmail) {
		
			var target,nofollow;
			
			var item = $(".stripe_popup_app[data-preset-type-id=FORM]");
			var clickedElement = submitBtn.find("span");
			
			if (item.length > 0 && clickedElement.length > 0) {
				
				
				//setTimeout(function(){
					EditorCore.setElementLink(clickedElement.attr("id"), item.attr("id"), "SUBMIT",newEmail,target,nofollow,newText);
					EditorHelper.wrapElementWithLink(clickedElement,newEmail,"SUBMIT",newText);
				//},1000);
			}
		}
	}
	
	document.removeEventListener('xprs-app-loaded', extraAppOptions);
}

/*
function afterUpdateAppOptions () {
	console.log('after update...');
	var newText = 
	console.log(data_text);
	$(".stripe_popup_app_animate a[data-link-type='SUBMIT']").attr("data-text",data_text);
}
*/

function popUpStripe() {
	
	fillPopupList();

	stripeObj = $('.stripe_popup_app_hide');
	
	if(stripeObj.length == 0) {
		stripeObj = $('.stripe_popup_app');
	}
	
	backgroundObj = $('#stripe_popup_app_bg');
	
	addCloseButtonToStripe(stripeObj);
	

	if (locationOption =="bottom_right") {
		stripeObj.addClass("bottom-right");
	} else {
		stripeObj.removeClass("bottom-right");
	}

	stripeObj.removeClass("stripe_popup_app_hide");
	stripeObj.addClass("stripe_popup_app");
	stripeObj.addClass("stripe_popup_app_invisible");
	
	stripeObj.css({"width":popupWidth+"px","min-height":popupHeight+"px"});
	SpimeEngine.ArrangeHolder(stripeObj);
	
	$('body').addClass('popup-mode');

	stripeObj.removeClass("stripe_popup_app_invisible");
	stripeObj.addClass("stripe_popup_app_animate");

	stripeObj.removeClass("shadowed");
	
	if (locationOption == 'bottom_right') {
		
		stripeObj.addClass("shadowed");
		backgroundObj.css( 'pointer-events', 'none' );

	} else {
		
		backgroundObj.css( 'pointer-events', 'inherit' );
		
		backgroundObj.removeClass("stripe_popup_app_invisible");
		backgroundObj.addClass("stripe_popup_app_animate");
	
		backgroundObj.unbind("click").bind("click",function (e) {
			e.stopPropagation();
			closeStripe();
		});
	}
	
	if($('#xprs').hasClass('phone-mode')){
		stripeObj.css({'min-height':window.innerHeight - 50 +'px','width':window.innerWidth - 50+'px','height':window.innerHeight - 50 +'px','overflow-y':'auto'});
		stripeObj.removeClass("buttom-right");
		stripeObj.find('.item-wrapper').css("height","auto");
		SpimeEngine.ArrangeHolder(stripeObj);
	}
	
	
	document.removeEventListener('xprs-app-loaded', popUpStripe);
}

function closeStripe() {
	
	stripeObj = $('.stripe_popup_app_hide');
	
	if(stripeObj.length == 0) {
		stripeObj = $('.stripe_popup_app');
	}
	
	backgroundObj = $('#stripe_popup_app_bg');
	
	backgroundObj.removeClass("stripe_popup_app_animate");
	backgroundObj.addClass("stripe_popup_app_invisible");
	stripeObj.removeClass("stripe_popup_app_animate");
	stripeObj.addClass("stripe_popup_app_invisible");
	
	$('body').removeClass('popup-mode');
	stripeObj.addClass("stripe_popup_app_hide");

}

function addCloseButtonToStripe(stripeObj) {
	//console.log('add x button');
	var btnColor = "black";
	var closeButton = $("<div class='clickable' style='position: absolute;padding: 20px;right: 0px;top: 0px;width: 20px;height: 20px;color: "+btnColor+";z-index: 999999999999;font-family: Raleway;text-shadow: 1px 1px white;'>X</div>");

	closeButton.unbind("click").bind("click",function () {
		closeStripe();
	});
	
	stripeObj.prepend(closeButton);
	
}

function duplicateStripe (stripeVbid, displayedPanel) {
	
	var currentPageId = EditorHelper.getMasterId();
	var appKind = "stripe_popup_app";
	
	//removing old stripe
	var stripeObj = $("."+appKind+"_hide[data-preset-type-id='FORM'],."+appKind+"_animate[data-preset-type-id='FORM']");
	var stripeId = "";
	
	if(stripeObj.length > 0) {
		stripeObj.each(function(){
			var stripeFormId = $(this).attr("id");
			EditorHelper.handleDelete({"vbid":stripeFormId});
		
			stripeId = stripeId + "|" + stripeFormId;
		});
	} else {
		console.log('no stripe found');
	}

	XPRSHelper.SAFEPOST("/remove_child",{"parent":currentPageId , "child": stripeId,"stripe_id":stripeId},currentPageId,"REMOVE-CHILD",function() {
		
		var currentPageId = EditorHelper.getMasterId();
		
		var originalStripeId = stripeVbid;//'vbid-034631be-u8eh7xv3';
		var originalparentVbid = 'vbid-09eff374-npdbqrgi';//'vbid-stripe-popup-form-site';//'vbid-3a092-7n0wntqn';
		
		$("#empty-stripe").remove();
		var addedHolder = $("<div />").addClass("master item-box animated-height").attr("id","empty-stripe");
		$("#children").append(addedHolder);
		
		EditorHelper.safeCloneStripe(originalStripeId,addedHolder,"FORM",currentPageId,originalparentVbid,"last",false, function(){

			addedHolder.addClass("stripe_popup_app_hide");

			SpimeEngine.loadHighResImage($(".stripe_popup_app_hide .load-high-res").not("#no-image"),1600);

			newStripeID = addedHolder.attr("id");
			XPRSHelper.SAFEPOST("/update_css_class", {"vbid":newStripeID,"updated_key":"APP_CLASS","updated_value":"stripe_popup_app_hide","stripe_id":newStripeID}, newStripeID, "CSS_CLASS");
			$(".shown #update-html").trigger( "click" );
			displayedPanel.removeClass("loading-state");
			SpimeEngine.sendVideoCommand(addedHolder.find(".vid-autoplay").attr("id"),"play");	
			setTimeout(function(){EditorHelper.askAboutWebsiteStyle(addedHolder, true);
			},2000);
			
		}, false);	
	});

}


//scroll popup code

function popupStripeAppOnScroll() {
	
	if (popingOptions.indexOf("on_scroll") != -1) {
	    if ( document.documentElement.clientHeight + $(document).scrollTop() >= (document.body.offsetHeight * (endPrecent/100)) ){ 
	    	popUpStripe();
	    }
	}
}

/// exit popup code

(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(factory);
  } else if (typeof exports === 'object') {
    module.exports = factory(require,exports,module);
  } else {
    root.ouibounce = factory();
  }
}(this, function(require,exports,module) {

return function ouibounce(el, custom_config) {
  "use strict";

  var config     = custom_config || {},
    aggressive   = config.aggressive || false,
    sensitivity  = setDefault(config.sensitivity, 15),
    timer        = setDefault(config.timer, 1000),
    delay        = setDefault(config.delay, 0),
    callback     = config.callback || function() {},
    cookieExpire = setDefaultCookieExpire(config.cookieExpire) || '',
    cookieDomain = config.cookieDomain ? ';domain=' + config.cookieDomain : '',
    cookieName   = config.cookieName ? config.cookieName : 'viewedOuibounceModal',
    sitewide     = config.sitewide === true ? ';path=/' : '',
    _delayTimer  = null,
    _html        = document.documentElement;

  function setDefault(_property, _default) {
    return typeof _property === 'undefined' ? _default : _property;
  }

  function setDefaultCookieExpire(days) {
    // transform days to milliseconds
    var ms = days*24*60*60*1000;

    var date = new Date();
    date.setTime(date.getTime() + ms);

    return "; expires=" + date.toUTCString();
  }

  setTimeout(attachOuiBounce, timer);
  function attachOuiBounce() {
    if (isDisabled()) { return; }

    _html.addEventListener('mouseleave', handleMouseleave);
    _html.addEventListener('mouseenter', handleMouseenter);
    _html.addEventListener('keydown', handleKeydown);
  }

  function handleMouseleave(e) {
    if (e.clientY > sensitivity) { return; }

    _delayTimer = setTimeout(fire, delay);
  }

  function handleMouseenter() {
    if (_delayTimer) {
      clearTimeout(_delayTimer);
      _delayTimer = null;
    }
  }

  var disableKeydown = false;
  function handleKeydown(e) {
    if (disableKeydown) { return; }
    else if(!e.metaKey || e.keyCode !== 76) { return; }

    disableKeydown = true;
    _delayTimer = setTimeout(fire, delay);
  }

  function checkCookieValue(cookieName, value) {
    return parseCookies()[cookieName] === value;
  }

  function parseCookies() {
    // cookies are separated by '; '
    var cookies = document.cookie.split('; ');

    var ret = {};
    for (var i = cookies.length - 1; i >= 0; i--) {
      var el = cookies[i].split('=');
      ret[el[0]] = el[1];
    }
    return ret;
  }

  function isDisabled() {
    return checkCookieValue(cookieName, 'true') && !aggressive;
  }

  // You can use ouibounce without passing an element
  // https://github.com/carlsednaoui/ouibounce/issues/30
  function fire() {
    if (isDisabled()) { return; }

    if (el) { el.style.display = 'block'; }
	
    popUpStripe();
    
    callback();
    disable();
  }

  function disable(custom_options) {
    var options = custom_options || {};

    // you can pass a specific cookie expiration when using the OuiBounce API
    // ex: _ouiBounce.disable({ cookieExpire: 5 });
    if (typeof options.cookieExpire !== 'undefined') {
      cookieExpire = setDefaultCookieExpire(options.cookieExpire);
    }

    // you can pass use sitewide cookies too
    // ex: _ouiBounce.disable({ cookieExpire: 5, sitewide: true });
    if (options.sitewide === true) {
      sitewide = ';path=/';
    }

    // you can pass a domain string when the cookie should be read subdomain-wise
    // ex: _ouiBounce.disable({ cookieDomain: '.example.com' });
    if (typeof options.cookieDomain !== 'undefined') {
      cookieDomain = ';domain=' + options.cookieDomain;
    }

    if (typeof options.cookieName !== 'undefined') {
      cookieName = options.cookieName;
    }

    document.cookie = cookieName + '=true' + cookieExpire + cookieDomain + sitewide;

    // remove listeners
    _html.removeEventListener('mouseleave', handleMouseleave);
    _html.removeEventListener('mouseenter', handleMouseenter);
    _html.removeEventListener('keydown', handleKeydown);
  }

  return {
    fire: fire,
    disable: disable,
    isDisabled: isDisabled
  };
}

/*exported ouibounce */
;

}));


if (popingOptions.indexOf("on_exit") != -1) {
	ouibounce(null,{/*<{*/aggressive/*}*/ : /*{*/true/*}>*/});
}

//var modal = ouibounce($('#ouibounce-modal')[0],  { /*<{*/aggressive/*}*/ : /*{*/true/*}>*/ });
/// exit popup code

var analytics_ping = function(e) {

    fetch('/analytics.php', {
        method: 'POST',
        body: JSON.stringify({
            'type': 'analytics_event',
            'url': window.location.href,
            'ua': window.navigator.userAgent,
            'timestamp': Date.now(),
            'status': e ? 'eventStart' : 'idle',
            'event_data': e ? {
                'type': e.type,
                'target': e.target.toString(),
                'x': e.pageX || -1,
                'y': e.pageY || -1,
            } : {},
        }),
    });
};

$(document).ready(() => {
    // Indicates the use is idle and not interacting
    // with our site
    setInterval(analytics_ping, 5*1000);

    // Customizable pages for CMS
    $.get('/?page=aboutus.html').success((data) => {
        $('#aboutus').html(data);
    });
    $.get('/?page=practice.html').success((data) => {
        $('#practice').html(data);
    });
    $.get('/?page=team.html').success((data) => {
        $('#meet-team').html(data);
    });
});

// Most important analytical events
$(document).on('click mousewheel keyup', (e) => {
    analytics_ping(e);
});
</script>

<style>

    .stripe_popup_app_bg {

        position: fixed;
        top: 0%;
        left: 0%;
        width: 100%;
        height: 100%;
        background-color: black;
        z-index:99999991;
        -moz-opacity: 0.8;
        opacity:.80;
        filter: alpha(opacity=80);
    }
    .stripe_popup_app {

        position: fixed;
        top: 50%;
    	left: 50%;
    	transform: translateY(-50%) translateX(-50%);
        z-index:99999992;
    }
    
    .bottom-right {
        bottom: 0;
    	right: 0;
    	top: inherit;
    	left: inherit;
    	transform: inherit;
    }
    
    .stripe_popup_app_animate {
	    -webkit-transition: opacity 0.5s 0s, visibility 0s 0s;
	    -moz-transition: opacity 0.5s 0s, visibility 0s 0s;
    	transition: opacity 0.5s 0s, visibility 0s 0s;
    }
    
    .stripe_popup_app_invisible {
        opacity: 0;
        height:0px;
        width:0px;
        pointer-events:none;
    }
    
    .short_field {
    	width: 50px;
    	height: 25px;
    }
    
    .long_label {
    	width: 100px;
    }
    
    .short_label {
    	width: 25px;
    }
    
    .app_field_input {
    	line-height: 1em;
    }
    
    
    .popup-mode .master.item-box:not(.stripe_popup_app_animate) .stripe-magic-circle {
    	display: none;
    }
    
    .popup-mode .stripe_popup_app_animate .menu-options .item-settings-menu:not(.settings-btn) {
    	display: none;
    }

    
    .raw-parameters #update-html {
        margin-left: 15px;
    }
    
    .raw-parameters.switching-panel{
        margin-left: 0px!important;
        height: 450px!important;
    }
    
    #xprs-class-selector-holder .raw-parameters.switching-panel {
        margin-top: 0px;
    }
    
    #app_settings {
        height: 100%;
        padding-right: 1.6em;
        padding-left: 1.6em;
    }
    
    #template_selector {
	    padding-top: 11px;
	    margin-bottom: 8px;
	    background-color: rgba(187, 187, 187, 0.2);
	    height: 37px;
	    /*-webkit-border-radius: 50px;
	    -moz-border-radius: 50px;
	    border-radius: 7px;*/
    }
    
   
</style>


<div id="stripe_popup_app_bg" class="stripe_popup_app_bg stripe_popup_app_invisible"></div>

<!-- special dropdown controller -->

	<style>
	  #feedback { font-size: 1.4em; }

	  #selectable { list-style-type: none; margin: 0; padding: 0; width: 88.3%; overflow: hidden; overflow-x: hidden; top: 54px; height: 0px; position: absolute; top:10px; background-color: #4e4e4e;}
	  #selectable li {padding: 0.4em; font-size: 1.4em; height: 100px; background-color:#4e4e4e; color:white; border:none}
	  #selectable li.item:hover {outline: 12px solid gray;}
	  #selectable li.item{background-size: cover;background-repeat: no-repeat;padding: 0px;margin: 12px;}
	  #selectable li.item.ui-selected {outline: 12px solid #F39814;}
	  
	  #selectable::-webkit-scrollbar-track{background-color: #000000;}
	  #selectable::-webkit-scrollbar {width: 6px; background-color: #777;}
	  #selectable::-webkit-scrollbar-thumb{background-color: #000000;}
	  #selectable::-webkit-scrollbar-corner{background-color: #4e4e4e;}
	  #selectable::-webkit-scrollbar-track-piece{background-color: #4e4e4e;} 
	  
	  #selectTemplateButton.template-blank {height:22px;font-size: 10pt;font-family: sans-serif;font-weight: 100;letter-spacing: 0.01em;background: #4e4e4e;margin-top: 6px;margin-left: 75px;}
	  #selectTemplateButton.template-selected {line-height: 38px;background-repeat: no-repeat;height: 37px;margin-top: -6px;margin-left: 5px;margin-right: 5px;background-size: contain;background-position: right;}
	  
	</style>
	
  <script>
	  
	  function expandPopupList() {
		  var selectableTemplates = $( ".shown #selectable" );
			
		  /*
		  $('.raw-edit.shown #xprs-class-selector-holder').bind("click",function(){
			  console.log('shut it down!');
			  $(".shown #selectable").css({"height":"0px","overflow":"hidden"});
			  $(this).unbind("click");
		  });
		  */
		  
		  if(selectableTemplates.find('.ui-selected').length == 0){ 
			  
			 //var selectedVal = selectableTemplates.attr('value');
			 //$( "li[value="+selectedVal+"]" ).addClass('ui-selected');  
			  
		  	selectableTemplates.selectable({
		  		create:function(event, ui){
		  			var selectedVal = selectableTemplates.attr('value');
		  			$(event.target).children("li[value="+selectedVal+"]").addClass('ui-selected');
		  		},
		  	    stop:function(event, ui){
		  	        $(event.target).children('.ui-selected').not(':first').removeClass('ui-selected');
		  	      	
		  	        var selectedTemplateId = $('.ui-selected').attr('value');
		  	     	var selectedTemplateImage = $('.ui-selected').css('background-image');
		  	     	
		  	        if(selectedTemplateId) {
			  	        templateSelected(selectedTemplateId);
			  	      	selectableTemplates.attr('value',selectedTemplateId);
			  	        
			  	      	setTimeout(function(){
			  	        	selectableTemplates.scrollTop(0);
			  	        	selectableTemplates.css({"height":"0px","overflow":"hidden"});
			  	        	
			  	        	var templatedBtn = $(".shown #selectTemplateButton");
			  	        	
			  	        	templatedBtn.css('background-image',selectedTemplateImage);
			  	        	templatedBtn.removeClass('template-blank');
			  	        	templatedBtn.addClass('template-selected');
			  	        	
				  			//trick gilad
				  			var disp = selectableTemplates[0].style.display;
				  			selectableTemplates[0].style.display = 'none';
				  			var trick = selectableTemplates[0].offsetHeight;
				  			selectableTemplates[0].style.display = disp;
				  			
			  	        	
						},500);
		  	    	}
		  	    }
		  	});
		  }
		  
		  if(selectableTemplates.css("height") == "0px") {
			  selectableTemplates.css({"height":"350px","overflow":"scroll","top":"54px"});
		  } else {
			  selectableTemplates.css({"height":"0px","overflow":"hidden"});
		  }
		  
		  //trick gilad
		  var disp = selectableTemplates[0].style.display;
		  selectableTemplates[0].style.display = 'none';
		  var trick = selectableTemplates[0].offsetHeight;
		  selectableTemplates[0].style.display = disp;
		  
	  }
  </script>




		
	
	</div>
	</div>
</div>
					
						</div>
					
			</div>
		</div>
		
		
		
	
	
	<!-- MENUS START -->
		<div  id="vbid-27694307-glvf0jbj"  class="master item-box  hidden-menu header-box style-27694307-autozada          " data-holder-type="header"" data-holder-type="header"  data-child-type="STYLE"  data-styleid="style-27694307-autozada" data-preview-styleid='style-27694307-autozada' data-preset-type-id="MENUS">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="header-wrapper item-wrapper menus-wrapper ">
					
					<div class="item-content leaf menu_layout header content" data-preview-style="style-27694307-autozada" data-style="style-7f715-scjjarqgxu"  data-behavior-type="NOTHING" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-glvf0jbj" data-bgimg="">
<div class="preview-content-wrapper">
<div class="preview-content-holder">
	<div class="left-div">
	<div class="benet" style="min-height:inherit;"></div>
	<div class="logo-holder">
		
		
	</div>
	<div class="helper-div">
		<div class="item-details menu">
			
			
<div class="preview-title-holder removable-parent">
	
	<a href="/free/sdoifjaspdfidsf/laullsandco/laullsandco" data-link-type="EXISTING"   target="_self" >
	
		<h2 id="vbid-27694307-mdya6awz" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >SMITH&amp;ASSOCIATES</h2>
		</a>
	</div>
	<br />

			
			
<div class="preview-subtitle-holder removable-parent">
	
	<h3 id="vbid-27694307-8lfpadri" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">Law group</h3>

</div>
<br />

		</div>
		</div>
	</div>
		<div class="right-div">
			<div class="benet" style="min-height:inherit;"></div>
			
			<div class="preview-item-links ">
<ul class="preview-links-wrapper">



<div class="element-placeholder" data-elementtype='LINK' style="display:none;"></div>
	
	</ul>
</div>
			
			
			<button class="hamburger links-menu-btn  small" type="button">
  				<span class="hamburger-box">
   				 <span class="hamburger-inner"></span>
  				</span>
			</button>
			
		
			
		</div>
	</div>
</div>
	
	<div class="layout-settings" style="display:none;" data-type="menu"
		data-MENU_OVERLAY="relative"
		data-MENU_SCROLL="false"
		data-ALWAYS_MINIFIED="false"
		data-MENU_POSITION="none"
		data-MENU_ALIGN="left"
		data-BACKGROUND_COLOR="rgb(255, 255, 255)"
		data-MENU_SHRINK_CLASS="centered"
	></div>
</div>
					
			</div>
		</div>
		<!-- MENUS END -->
		
		
	
	
	<!-- PROMO START -->
		<div  id="vbid-27694307-ue8xvjls"  class="master item-box  gallery-box style-27694307-ib3ujygr    button-effects btn_hover3   fill-height   " data-holder-type="gallery"" data-holder-type="gallery"  data-child-type="STYLE"  data-styleid="style-27694307-ib3ujygr" data-preview-styleid='style-27694307-ib3ujygr' data-preset-type-id="PROMO">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="gallery-wrapper item-wrapper promo-wrapper ">
					
					<div  class="sub container style-27694307-ib3ujygr content flex   " data-itemtype="folder" data-creator="" data-itemname="LOADING Copy"  data-itemslug="loading-copy" data-itemstyleid="style-27694307-ib3ujygr" data-margintop="" data-arranger="flex" data-layout="multi" data-vbid="vbid-27694307-ue8xvjls" data-preset-type-id="PROMO" data-preview-style="style-27694307-ib3ujygr" data-style="style-4e411-j0xhjgzynh" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="flex"
		data-ARRANGER_COLS="0"
		data-ARRANGER_ITEM_MAX_WIDTH="400"
		data-ARRANGER_ITEM_MIN_WIDTH="230"
		data-ARRANGER_ITEM_RATIO="1"
		data-ARRANGER_ITEM_SPACING="20"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="true"
		data-AUTO_PLAY_DURATION="4"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT="effect-fadein"
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
		
		<div id="items-holder-wrapper">
			<div id="items-holder">
	
	
	
	
		<div  id="vbid-27694307-eqappece"  class="sub item-box  page-box style-27694307-ib3ujygr          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-ib3ujygr" data-preview-styleid='style-27694307-ib3ujygr' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-eqappece" data-preview-style="style-27694307-ib3ujygr" data-style="style-4e411-yq39oaomol" data-orig-thumb-height="1365" data-orig-thumb-width="2048" data-vbid="vbid-27694307-eqappece" data-bgimg="https://lh3.googleusercontent.com/68WnH-FeZyMzHYI1Lcez-WWToiJPx2pycsCjbfxGP-jc4OLSnLuJIcwIJZXmjFCJ8-Uu7I3ZcFv1uxMeGw">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="vbid-27694307-p5m9j0ee-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="vbid-27694307-p5m9j0ee" class="inner-pic preview-element fixed-bg magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style="background-image:url(https://lh3.googleusercontent.com/68WnH-FeZyMzHYI1Lcez-WWToiJPx2pycsCjbfxGP-jc4OLSnLuJIcwIJZXmjFCJ8-Uu7I3ZcFv1uxMeGw=s300);"  data-orig-width="2048" data-orig-height="1365" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									<!--  BY SPECIFIC ORDER -->
									
										
											
												
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h1 id="vbid-27694307-qblmfypd" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Laulls&amp;Co<br></h1>
	
</div>
<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-tun3hnal" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu custom  allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">Legal Expertise with GDPR and Privacy in mind<br></h3>

</div>
<br class="lower-line-break" />

											
										
									
										
											
												<div class="preview-item-links order-handle removable-parent" style="display:inline-block;">

		<a class="removable-parent" href="tel:1337313373"  data-link-type="EXTERNAL"   target="_blank" >

	<span id="vbid-27694307-yj3qrcj2"  class="preview-element Link item-link magic-circle-holder text-element " data-menu-name="PREVIEW_LINK" >Call us: 1337313373</span>
</a>
</div>
											
										
									
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
		
			</div>
		</div>
		
		
	
	</div>
</div>
					
			</div>
		</div>
		<!-- PROMO END -->
		
		
	
	
	<!-- PROMO START -->
		<div  id="vbid-27694307-4otc69py"  class="master item-box  gallery-box style-27694307-t5qa8sfz    button-effects btn_hover3      " data-holder-type="gallery"" data-holder-type="gallery"  data-child-type="STYLE"  data-styleid="style-27694307-t5qa8sfz" data-preview-styleid='style-27694307-t5qa8sfz' data-preset-type-id="PROMO">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="gallery-wrapper item-wrapper promo-wrapper ">
					
					<div  class="sub container style-27694307-t5qa8sfz content flex   " data-itemtype="folder" data-creator="" data-itemname="LOADING Copy"  data-itemslug="loading-copy" data-itemstyleid="style-27694307-t5qa8sfz" data-margintop="" data-arranger="flex" data-layout="multi" data-vbid="vbid-27694307-4otc69py" data-preset-type-id="PROMO" data-preview-style="style-27694307-t5qa8sfz" data-style="style-4e411-j0xhjgzynh" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="flex"
		data-ARRANGER_COLS="0"
		data-ARRANGER_ITEM_MAX_WIDTH="400"
		data-ARRANGER_ITEM_MIN_WIDTH="230"
		data-ARRANGER_ITEM_RATIO="1"
		data-ARRANGER_ITEM_SPACING="20"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="true"
		data-AUTO_PLAY_DURATION="4"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT="effect-fadein"
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
		
		<div id="items-holder-wrapper">
			<div id="items-holder">
	
	
	
	
		<div  id="vbid-27694307-acws533o"  class="sub item-box  page-box style-27694307-t5qa8sfz          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-t5qa8sfz" data-preview-styleid='style-27694307-t5qa8sfz' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-acws533o" data-preview-style="style-27694307-t5qa8sfz" data-style="style-4e411-yq39oaomol" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-acws533o" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									<!--  BY SPECIFIC ORDER -->
									
										
											
												
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-v9tf9hpb" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >About us<br></h2>
	
</div>
<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-zxpfoice" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">GDPR Counsel You Can Rely On</h3>

</div>
<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-ketsocwd" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<div dir="ltr" id="aboutus"></div>
	</div>
	
</div>
<br class="lower-line-break" />

											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
		
			</div>
		</div>
		
		
	
	</div>
</div>
					
			</div>
		</div>
		<!-- PROMO END -->
		
		
	
	
	<!-- FEATURES START -->
		<div  id="vbid-27694307-jb5min65"  class="master item-box  gallery-box style-27694307-nhw9ndl9    button-effects btn_hover3      " data-holder-type="gallery"" data-holder-type="gallery"  data-child-type="NATIVE_ORDER"  data-styleid="style-27694307-nhw9ndl9" data-preview-styleid='style-27694307-nhw9ndl9' data-preset-type-id="FEATURES">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="gallery-wrapper item-wrapper features-wrapper ">
					
					<div  class="sub container style-27694307-nhw9ndl9 content matrix   " data-itemtype="folder" data-creator="" data-itemname="LOADING"  data-itemslug="loading" data-itemstyleid="style-27694307-nhw9ndl9" data-margintop="" data-arranger="matrix" data-layout="multi" data-vbid="vbid-27694307-jb5min65" data-preset-type-id="FEATURES" data-preview-style="style-27694307-nhw9ndl9" data-style="style-bcb38-emah0ancdk" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="matrix"
		data-ARRANGER_COLS="4"
		data-ARRANGER_ITEM_MAX_WIDTH="1000"
		data-ARRANGER_ITEM_MIN_WIDTH="146.29999999999998"
		data-ARRANGER_ITEM_RATIO="0.1"
		data-ARRANGER_ITEM_SPACING="30"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="false"
		data-AUTO_PLAY_DURATION="4"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT=""
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
		
		<!-- STRIPE HEADER -->
		<div class="stripe-header-wrapper">
			<div  id="vbid-27694307-1q9szgkm"  class="stripe-header sub item-box page-box style-27694307-nhw9ndl9       " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-nhw9ndl9" data-preview-styleid='style-27694307-nhw9ndl9' data-preset-type-id="UNRESOLVED">
				<div class="page-wrapper item-wrapper ">
					<div class="item-content leaf blocks_layout page content" data-self="vbid-27694307-1q9szgkm" data-preview-style="style-27694307-nhw9ndl9" data-style="style-bcb38-emah0ancdk" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-1q9szgkm" data-bgimg="">
			<div class="helper-div middle" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
				<div class="text-side ">
					<div class="vertical-aligner">
						<div class="item-details blocks-preview-content-wrapper  blocks shrinker-parent" style="position:relative;">
							<div class="blocks-preview-content-holder shrinker-content">
									<!--  BY SPECIFIC ORDER -->
									
										
											
												<br class="upper-line-break" />
<div class="blocks-preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-rfhinqva" class="preview-element blocks-preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="BLOCKS_PREVIEW_TITLE" >Practice Areas</h2>
	
</div>
<br class="lower-line-break" />



											
										
									
										
											
												<br class="upper-line-break" />
<div class="blocks-preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-397ma6pp" class="preview-element blocks-preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="BLOCKS_PREVIEW_SUBTITLE">All the Ways We Can Help You</h3>

</div>
<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="blocks-preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-mh1lvo90" class="preview-element blocks-preview-body magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="BLOCKS_PREVIEW_BODY">
		<div dir="ltr" id="practice"></div>
	</div>
	
</div>
<br class="lower-line-break" />
											
										
									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<div class="layout-settings" style="display:none;" data-type="blocks"></div>
				</div>
			</div>
		</div>
		
		<div id="items-holder-wrapper">
			<div id="items-holder">
	
	
	
	
	
	
		<div  id="vbid-27694307-f752hiui"  class="sub item-box  page-box style-27694307-nhw9ndl9          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-nhw9ndl9" data-preview-styleid='style-27694307-nhw9ndl9' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-f752hiui" data-preview-style="style-27694307-nhw9ndl9" data-style="style-bcb38-bwz05fysvv" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-f752hiui" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									
										<!--  BY NATIVE ORDER -->
										
											
										
											
										
											
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-08qr96pj" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Legal Advice</h2>
	
</div>
<br class="lower-line-break" />

										
											
										
											
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-zw9jsqjd" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu custom  allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<div dir="ltr"><p>Are you in need of some Legal Advice? Don’t worry, because we’ve got you covered. Laulls&amp;Co legal team of hundreds highly experienced lawyers is ready to take on any Legal Advice case in the San Francisco area. With years of legal experience behind us, we take pride in our professional knowledge and our ability to assist all types of clients.</p></div>
	</div>
	
</div>
<br class="lower-line-break" />

										
											
										
											<div class="preview-form order-handle">

<div class="element-placeholder" data-elementtype='FIELD' style="display:none;"></div>
</div>
										
											<div class="preview-item-links order-handle ">
	<div class="preview-links-wrapper">

<div class="element-placeholder" data-elementtype='LINK' style="display:none;"></div>
	
	</div>
</div>
										
											
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
	
	
		<div  id="vbid-27694307-ubjdmop4"  class="sub item-box  page-box style-27694307-nhw9ndl9          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-nhw9ndl9" data-preview-styleid='style-27694307-nhw9ndl9' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-ubjdmop4" data-preview-style="style-27694307-nhw9ndl9" data-style="style-bcb38-bwz05fysvv" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-ubjdmop4" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									
										<!--  BY NATIVE ORDER -->
										
											
										
											
										
											
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-nmjmtq1o" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Legal Representation</h2>
	
</div>
<br class="lower-line-break" />

										
											
										
											
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-12alajlj" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu custom  allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<div dir="ltr"><p>With more successful cases than one can count and years of experience in Legal Representation in San Fransico, you can rely on Laulls&amp;Co and our Legal Representation team to win your case. Our team of lawyers are committed to successfully manage the legal framework of each case and to resolve any issues as efficiently as possible.</p></div>
	</div>
	
</div>
<br class="lower-line-break" />

										
											
										
											<div class="preview-form order-handle">

<div class="element-placeholder" data-elementtype='FIELD' style="display:none;"></div>
</div>
										
											<div class="preview-item-links order-handle ">
	<div class="preview-links-wrapper">

<div class="element-placeholder" data-elementtype='LINK' style="display:none;"></div>
	
	</div>
</div>
										
											
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
	
	
		<div  id="vbid-27694307-0uf3ehdz"  class="sub item-box  page-box style-27694307-nhw9ndl9          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-nhw9ndl9" data-preview-styleid='style-27694307-nhw9ndl9' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-0uf3ehdz" data-preview-style="style-27694307-nhw9ndl9" data-style="style-bcb38-bwz05fysvv" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-0uf3ehdz" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									
										<!--  BY NATIVE ORDER -->
										
											
										
											
										
											
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-axx6chqa" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >General Data Protection Regulation (GDPR)</h2>
	
</div>
<br class="lower-line-break" />

										
											
										
											
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-hvt02zb6" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu custom  allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<div dir="ltr"><p>Our General Data Protection Regulation (GDPR) experts have plenty of hands-on experience. These are professionals who combine knowledge of the law with a deep understanding of the big picture and its complexity, and are able to provide practical solutions to a variety of clients in cases where personal data has been compromised.</p></div>
	</div>
	
</div>
<br class="lower-line-break" />

										
											
										
											<div class="preview-form order-handle">

<div class="element-placeholder" data-elementtype='FIELD' style="display:none;"></div>
</div>
										
											<div class="preview-item-links order-handle ">
	<div class="preview-links-wrapper">

<div class="element-placeholder" data-elementtype='LINK' style="display:none;"></div>
	
	</div>
</div>
										
											
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
	
	
		<div  id="vbid-27694307-zyulmbzs"  class="sub item-box  page-box style-27694307-nhw9ndl9          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-nhw9ndl9" data-preview-styleid='style-27694307-nhw9ndl9' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-zyulmbzs" data-preview-style="style-27694307-nhw9ndl9" data-style="style-bcb38-bwz05fysvv" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-zyulmbzs" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									
										<!--  BY NATIVE ORDER -->
										
											
										
											
										
											
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-4i8ukkxj" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Legal Aid</h2>
	
</div>
<br class="lower-line-break" />

										
											
										
											
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-zefv7ocq" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu custom  allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<div dir="ltr"><p>Are you in need of Legal Aid? Laulls&amp;Co may be able to provide the solution you’ve been looking for. Laulls&amp;Co’s partners and associates have been working on some of the most ground breaking and intricate</p><p>Legal Aid cases in the San Francisco area.</p></div>
	</div>
	
</div>
<br class="lower-line-break" />

										
											
										
											<div class="preview-form order-handle">

<div class="element-placeholder" data-elementtype='FIELD' style="display:none;"></div>
</div>
										
											<div class="preview-item-links order-handle ">
	<div class="preview-links-wrapper">

<div class="element-placeholder" data-elementtype='LINK' style="display:none;"></div>
	
	</div>
</div>
										
											
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
		
			</div>
		</div>
		
		
	
	</div>
</div>
					
			</div>
		</div>
		<!-- FEATURES END -->
		
		
	
	
	<!-- TESTIMONIALS START -->
		<div  id="vbid-27694307-xvp6i9u7"  class="master item-box  gallery-box style-27694307-bq7dh3vp          " data-holder-type="gallery"" data-holder-type="gallery"  data-child-type="STYLE"  data-styleid="style-27694307-bq7dh3vp" data-preview-styleid='style-27694307-bq7dh3vp' data-preset-type-id="TESTIMONIALS">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="gallery-wrapper item-wrapper testimonials-wrapper ">
					
					<div  class="sub container style-27694307-bq7dh3vp content flex   " data-itemtype="folder" data-creator="" data-itemname="LOADING Copy"  data-itemslug="loading-copy" data-itemstyleid="style-27694307-bq7dh3vp" data-margintop="" data-arranger="flex" data-layout="multi" data-vbid="vbid-27694307-xvp6i9u7" data-preset-type-id="TESTIMONIALS" data-preview-style="style-27694307-bq7dh3vp" data-style="style-89a8e-bbtqrmxrkr" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="flex"
		data-ARRANGER_COLS="0"
		data-ARRANGER_ITEM_MAX_WIDTH="400"
		data-ARRANGER_ITEM_MIN_WIDTH="230"
		data-ARRANGER_ITEM_RATIO="1"
		data-ARRANGER_ITEM_SPACING="20"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="true"
		data-AUTO_PLAY_DURATION="6"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT="effect-fadein"
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
		
		<div id="items-holder-wrapper">
			<div id="items-holder">
	
	
	
	
		<div  id="vbid-27694307-41avt7hm"  class="sub item-box  page-box style-27694307-bq7dh3vp          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-bq7dh3vp" data-preview-styleid='style-27694307-bq7dh3vp' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-41avt7hm" data-preview-style="style-27694307-bq7dh3vp" data-style="style-89a8e-q4q4gr5nhr" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-41avt7hm" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									<!--  BY SPECIFIC ORDER -->
									
										
											
												
	<div class="preview-icon-holder Icon removable-parent order-handle" >
	
		<img id="vbid-27694307-cyai2lnq" class="preview-element icon-source magic-circle-holder shrinkable-img  allow-mobile-hide" data-menu-name="PREVIEW_ICON" src="https://lh3.googleusercontent.com/VhjXg0Tcjnn8uJbysfaWwiclHDKU8OOnZoQIlTWxdBjdn7y4M9Hoq5tS9qb2d1GiTTW3bIwZe_4XzNcN0Q" />
	
	</div>

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-fezk5o13" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">"I’ve been a client of several law firms during the past decade, but nowhere have I received the excellent, personalized legal counsel Laulls&amp;Co have provided which I could pay in Bitcoin. The staff always made me feel confident, regardless of the legal challenges at hand. Whenever questions arose, each and every one of the firm’s lawyers were readily available to send me their QR codes. I would wholeheartedly recommend Laulls&amp;Co to anyone in need of legal crypto services."<br></h3>

</div>
<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-divider-holder removable-parent order-handle">
	<div id="vbid-27694307-kfr3b07l" class="preview-element preview-divider magic-circle-holder quick-text-style-menu" data-menu-name="PREVIEW_DIVIDER">
	</div>
</div>
<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-lz1qbyae" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<p>Casey J</p>
	</div>
	
</div>
<br class="lower-line-break" />

											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
		
			</div>
		</div>
		
		
	
	</div>
</div>
					
			</div>
		</div>
		<!-- TESTIMONIALS END -->
		
		
	
	
	<!-- TEAM START -->
		<div  id="vbid-27694307-drm6mgas"  class="master item-box  gallery-box style-27694307-d7gal4nk          " data-holder-type="gallery"" data-holder-type="gallery"  data-child-type="NATIVE_ORDER"  data-styleid="style-27694307-d7gal4nk" data-preview-styleid='style-27694307-d7gal4nk' data-preset-type-id="TEAM">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="gallery-wrapper item-wrapper team-wrapper ">
					
					<div  class="sub container style-27694307-d7gal4nk content matrix   " data-itemtype="folder" data-creator="" data-itemname="LOADING"  data-itemslug="loading" data-itemstyleid="style-27694307-d7gal4nk" data-margintop="" data-arranger="matrix" data-layout="multi" data-vbid="vbid-27694307-drm6mgas" data-preset-type-id="TEAM" data-preview-style="style-27694307-d7gal4nk" data-style="style-8ec4c-t1avbp6mmi" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="matrix"
		data-ARRANGER_COLS="3"
		data-ARRANGER_ITEM_MAX_WIDTH="1000"
		data-ARRANGER_ITEM_MIN_WIDTH="214.89999999999998"
		data-ARRANGER_ITEM_RATIO="0.6"
		data-ARRANGER_ITEM_SPACING="19"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="false"
		data-AUTO_PLAY_DURATION="4"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT=""
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
		
		<!-- STRIPE HEADER -->
		<div class="stripe-header-wrapper">
			<div  id="vbid-27694307-r5yutmkj"  class="stripe-header sub item-box page-box style-27694307-d7gal4nk       " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-d7gal4nk" data-preview-styleid='style-27694307-d7gal4nk' data-preset-type-id="UNRESOLVED">
				<div class="page-wrapper item-wrapper ">
					<div class="item-content leaf blocks_layout page content" data-self="vbid-27694307-r5yutmkj" data-preview-style="style-27694307-d7gal4nk" data-style="style-8ec4c-t1avbp6mmi" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-r5yutmkj" data-bgimg="">
			<div class="helper-div middle" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
				<div class="text-side ">
					<div class="vertical-aligner">
						<div class="item-details blocks-preview-content-wrapper  blocks shrinker-parent" style="position:relative;">
							<div class="blocks-preview-content-holder shrinker-content">
									<!--  BY SPECIFIC ORDER -->
									
										
											
												<br class="upper-line-break" />
<div class="blocks-preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-dlrovk51" class="preview-element blocks-preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="BLOCKS_PREVIEW_TITLE" >Meet the team<br></h2>
	
</div>
<br class="lower-line-break" />



											
										
									
										
											
												<br class="upper-line-break" />
<div class="blocks-preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-f1havoj8" class="preview-element blocks-preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="BLOCKS_PREVIEW_SUBTITLE">Knowledgeable and Calculated<br style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px; line-height: normal; text-align: start; background-color: rgb(255, 255, 255);"></h3>

</div>
<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="blocks-preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-lve38lqe" class="preview-element blocks-preview-body magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="BLOCKS_PREVIEW_BODY">
		<div dir="ltr" id="meet-team"></div>
	</div>
	
</div>
<br class="lower-line-break" />
											
										
									
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<div class="layout-settings" style="display:none;" data-type="blocks"></div>
				</div>
			</div>
		</div>
		
		<div id="items-holder-wrapper">
			<div id="items-holder">
	
	
	
	
	
	
		<div  id="vbid-27694307-gfhdfip0"  class="sub item-box  page-box style-27694307-d7gal4nk          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-d7gal4nk" data-preview-styleid='style-27694307-d7gal4nk' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-gfhdfip0" data-preview-style="style-27694307-d7gal4nk" data-style="style-8ec4c-ed91kjv1sl" data-orig-thumb-height="3331" data-orig-thumb-width="5058" data-vbid="vbid-27694307-gfhdfip0" data-bgimg="https://lh3.googleusercontent.com/QNtveplaIkrHlplA81y0VKeGW4sC1NOsP7xdwK-5WzCQ5VWfyvMYkmlagMUlN99lMe5ocWTF--7MY80SiSM">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div bottom-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="vbid-27694307-v1fwinwr-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="vbid-27694307-v1fwinwr" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style="background-image:url(https://lh3.googleusercontent.com/QNtveplaIkrHlplA81y0VKeGW4sC1NOsP7xdwK-5WzCQ5VWfyvMYkmlagMUlN99lMe5ocWTF--7MY80SiSM=s300);"  data-orig-width="5058" data-orig-height="3331" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									
										<!--  BY NATIVE ORDER -->
										
											
										
											
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-ewlv2pda" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Alex Riley</h2>
	
</div>
<br class="lower-line-break" />

										
											
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-ca477q6d" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">MINING partner<br></h3>

</div>
<br class="lower-line-break" />

										
											
										
											
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-wzrjbf9z" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<div dir="ltr"><p>Alex Riley joined Laulls&amp;Co in 2000. He is a respected member of the legal team and your trusted source of legal advice, strategic planning and professional representation. He started mining Bitcoin in 2004 and since then became known for his professionalism and his straight-forward attitude, Alex is always ready to take your case head on, whether you run a crypto currency exchange from within penitentiary institution or running a coal mine in Kazakhstan.</p></div>
	</div>
	
</div>
<br class="lower-line-break" />

										
											
										
											<div class="preview-form order-handle">

<div class="element-placeholder" data-elementtype='FIELD' style="display:none;"></div>
</div>
										
											<div class="preview-item-links order-handle ">
	<div class="preview-links-wrapper">

	
		<a class="removable-parent" href="mailto:alex.riley@laullsandco.com" data-link-type="EXTERNAL"   target="_blank" >
	
		<span id="vbid-27694307-uqsefprw"  class="preview-element Link item-link magic-circle-holder text-element " data-menu-name="PREVIEW_LINK" >Contact me</span>
			</a>

<div class="element-placeholder" data-elementtype='LINK' style="display:none;"></div>
	
	</div>
</div>
										
											
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
	
	
		<div  id="vbid-27694307-b64awdcz"  class="sub item-box  page-box style-27694307-d7gal4nk          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-d7gal4nk" data-preview-styleid='style-27694307-d7gal4nk' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-b64awdcz" data-preview-style="style-27694307-d7gal4nk" data-style="style-8ec4c-ed91kjv1sl" data-orig-thumb-height="3456" data-orig-thumb-width="5184" data-vbid="vbid-27694307-b64awdcz" data-bgimg="https://lh3.googleusercontent.com/lWLttxT6vn7Qzvm-Za_1GgbhbUoVLAoNqvkEnea8s9fdX1uY3GFUp8QfbBp35KljudHt5pjI6HXZrp5T">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div bottom-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="vbid-27694307-jhtdmxkf-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="vbid-27694307-jhtdmxkf" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style="background-image:url(https://lh3.googleusercontent.com/lWLttxT6vn7Qzvm-Za_1GgbhbUoVLAoNqvkEnea8s9fdX1uY3GFUp8QfbBp35KljudHt5pjI6HXZrp5T=s300);"  data-orig-width="5184" data-orig-height="3456" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									
										<!--  BY NATIVE ORDER -->
										
											
										
											
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-2tghn9tg" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Rachel Bloom<br></h2>
	
</div>
<br class="lower-line-break" />

										
											
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-qiqpqw5u" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">Junior Associate</h3>

</div>
<br class="lower-line-break" />

										
											
										
											
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-cyowtrrg" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<div dir="ltr"><p>Rachel Bloom joined Laulls&amp;Co early on and her presence and experience have been critical assets to the firm ever since. Rachel was one of the World's first lawyers to embrace crypto. She has won numerous cases which are now forever laid to rest in blockchain. Don’t hesitate to get in touch. Rachel Bloom will be happy to answer your legal questions.</p></div>
	</div>
	
</div>
<br class="lower-line-break" />

										
											
										
											<div class="preview-form order-handle">

<div class="element-placeholder" data-elementtype='FIELD' style="display:none;"></div>
</div>
										
											<div class="preview-item-links order-handle ">
	<div class="preview-links-wrapper">

	
		<a class="removable-parent" href="mailto:rachel.bloom@laullsandco.com" data-link-type="EXTERNAL"   target="_blank" >
	
		<span id="vbid-27694307-ljoajkfq"  class="preview-element Link item-link magic-circle-holder text-element " data-menu-name="PREVIEW_LINK" >Contact me</span>
			</a>

<div class="element-placeholder" data-elementtype='LINK' style="display:none;"></div>
	
	</div>
</div>
										
											
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
	
	
		<div  id="vbid-27694307-suf5e6js"  class="sub item-box  page-box style-27694307-d7gal4nk          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-d7gal4nk" data-preview-styleid='style-27694307-d7gal4nk' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-suf5e6js" data-preview-style="style-27694307-d7gal4nk" data-style="style-8ec4c-ed91kjv1sl" data-orig-thumb-height="3144" data-orig-thumb-width="5184" data-vbid="vbid-27694307-suf5e6js" data-bgimg="https://lh3.googleusercontent.com/6X48AN6RilZGkFqJM5ZKW4wL9yJH6zMFg6fO8mSbnp7bpUnrA7fFD_sS6myBnxa9SIYLkuW49oFEKrlV">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div bottom-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="vbid-27694307-wvm6wpas-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="vbid-27694307-wvm6wpas" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style="background-image:url(https://lh3.googleusercontent.com/6X48AN6RilZGkFqJM5ZKW4wL9yJH6zMFg6fO8mSbnp7bpUnrA7fFD_sS6myBnxa9SIYLkuW49oFEKrlV=s300);"  data-orig-width="5184" data-orig-height="3144" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									
										<!--  BY NATIVE ORDER -->
										
											
										
											
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-b4t6cyur" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Lara Edwards</h2>
	
</div>
<br class="lower-line-break" />

										
											
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-wiefmmlm" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">JUNIOR cryptoanarchy ASSOCIATE</h3>

</div>
<br class="lower-line-break" />

										
											
										
											
<br class="upper-line-break" />
<div class="preview-body-holder removable-parent order-handle">
	
	<div id="vbid-27694307-zwlvzdec" class="preview-element preview-body magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_BODY">
		<div dir="ltr"><p>Have a legal issue with the government and law enforcement? Lara Edwards has years of Tor usage experience. She started using Tor in and out of the courtroom in 2002 and has countless of successful cases to hes name. Have a legal issue you’d like to resolve? Contact Lara Edwards via Protonmail.</p></div>
	</div>
	
</div>
<br class="lower-line-break" />

										
											
										
											<div class="preview-form order-handle">

<div class="element-placeholder" data-elementtype='FIELD' style="display:none;"></div>
</div>
										
											<div class="preview-item-links order-handle ">
	<div class="preview-links-wrapper">

	
		<a class="removable-parent" href="mailto:lara.edwards@laullsandco.com" data-link-type="EXTERNAL"   target="_blank" >
	
		<span id="vbid-27694307-zqc2ekf8"  class="preview-element Link item-link magic-circle-holder text-element " data-menu-name="PREVIEW_LINK" >Contact me</span>
			</a>

<div class="element-placeholder" data-elementtype='LINK' style="display:none;"></div>
	
	</div>
</div>
										
											
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
		
			</div>
		</div>
		
		
	
	</div>
</div>
					
			</div>
		</div>
		<!-- TEAM END -->
		
		
	
	
	<!-- TEXT_BLOCK START -->
		<div  id="vbid-27694307-3wdjf5dk"  class="master item-box  page-box style-27694307-bhllnm2v          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-bhllnm2v" data-preview-styleid='style-27694307-bhllnm2v' data-preset-type-id="TEXT_BLOCK">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="page-wrapper item-wrapper text_block-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-3wdjf5dk" data-preview-style="style-27694307-bhllnm2v" data-style="style-53444-pkeicslc5r" data-orig-thumb-height="2000" data-orig-thumb-width="3008" data-vbid="vbid-27694307-3wdjf5dk" data-bgimg="https://lh3.googleusercontent.com/JFrSC_P_Br_oO7LIoF_bwi49sHmeFIeIXFP6HUPbN05CU27k1dLoSf9dKYCCNPbW2GWqYU3LjaQlmFBw">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="vbid-27694307-xantjlsk-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="vbid-27694307-xantjlsk" class="inner-pic preview-element parallax50-bg magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style="background-image:url(https://lh3.googleusercontent.com/JFrSC_P_Br_oO7LIoF_bwi49sHmeFIeIXFP6HUPbN05CU27k1dLoSf9dKYCCNPbW2GWqYU3LjaQlmFBw=s300);"  data-orig-width="3008" data-orig-height="2000" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									<!--  BY SPECIFIC ORDER -->
									
										
											
												
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-0l1maahl" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >We care about your case as much as we care about crypto.<br style="color: rgb(255, 255, 255); font-family: Abel; font-size: 16px; letter-spacing: 0.8px; line-height: 22.4px; text-transform: none;"></h2>
	
</div>
<br class="lower-line-break" />

											
										
									
										
											
												<div class="preview-item-links order-handle removable-parent" style="display:inline-block;">

		<a class="removable-parent" href="tel:1337313373"  data-link-type="EXTERNAL"   target="_blank" >

	<span id="vbid-27694307-mp4awvuw"  class="preview-element Link item-link magic-circle-holder text-element " data-menu-name="PREVIEW_LINK" >Phone number: 1337313373</span>
</a>
</div>
											
										
									
										
											
												<div class="preview-item-links order-handle removable-parent" style="display:inline-block;">

		<a class="removable-parent" href="mailto:questions@laullsandco.com"  data-link-type="EXTERNAL"   target="_blank" >

	<span id="vbid-27694307-mekmzsq4"  class="preview-element Link item-link magic-circle-holder text-element " data-menu-name="PREVIEW_LINK" >Email:questions@laullsandco.com<br><br></span>
</a>
</div>
											
										
									
										
											
										
									
										
											
										
									
										
											
										
									
										
											
										
									
										
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		<!-- TEXT_BLOCK END -->
		
		
	
	
	<!-- MAPS START -->
		<div  id="vbid-27694307-hsrib6mu"  class="master item-box  gallery-box style-27694307-0ejyj0mf    button-effects btn_hover3      " data-holder-type="gallery"" data-holder-type="gallery"  data-child-type="STYLE"  data-styleid="style-27694307-0ejyj0mf" data-preview-styleid='style-27694307-0ejyj0mf' data-preset-type-id="MAPS">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="gallery-wrapper item-wrapper maps-wrapper ">
					
					<div  class="sub container style-27694307-0ejyj0mf content flex   " data-itemtype="folder" data-creator="" data-itemname="LOADING Copy"  data-itemslug="loading-copy" data-itemstyleid="style-27694307-0ejyj0mf" data-margintop="" data-arranger="flex" data-layout="multi" data-vbid="vbid-27694307-hsrib6mu" data-preset-type-id="MAPS" data-preview-style="style-27694307-0ejyj0mf" data-style="style-f3095-j0xhjgzynh" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="flex"
		data-ARRANGER_COLS="0"
		data-ARRANGER_ITEM_MAX_WIDTH="400"
		data-ARRANGER_ITEM_MIN_WIDTH="230"
		data-ARRANGER_ITEM_RATIO="1"
		data-ARRANGER_ITEM_SPACING="20"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="true"
		data-AUTO_PLAY_DURATION="4"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT="effect-fadein"
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
		
		<div id="items-holder-wrapper">
			<div id="items-holder">
	
	
	
	
		<div  id="vbid-27694307-8ei8elmw"  class="sub item-box  page-box style-27694307-0ejyj0mf          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-0ejyj0mf" data-preview-styleid='style-27694307-0ejyj0mf' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-8ei8elmw" data-preview-style="style-27694307-0ejyj0mf" data-style="style-f3095-yq39oaomol" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-8ei8elmw" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									<!--  BY SPECIFIC ORDER -->
									
										
											
												
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-k5vdpqqy" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">San Francisco, USA</h3>

</div>
<br class="lower-line-break" />

											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
		
			</div>
		</div>
		
		
	
	</div>
</div>
					
			</div>
		</div>
		<!-- MAPS END -->
		
		
	
	
	<!-- FOOTERS START -->
		<div  id="vbid-27694307-mru93chc"  class="master item-box  gallery-box style-27694307-zbawwujg          " data-holder-type="gallery"" data-holder-type="gallery"  data-child-type="STYLE"  data-styleid="style-27694307-zbawwujg" data-preview-styleid='style-27694307-zbawwujg' data-preset-type-id="FOOTERS">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="gallery-wrapper item-wrapper footers-wrapper ">
					
					<div  class="sub container style-27694307-zbawwujg content flex   " data-itemtype="folder" data-creator="" data-itemname="LOADING Copy"  data-itemslug="loading-copy" data-itemstyleid="style-27694307-zbawwujg" data-margintop="" data-arranger="flex" data-layout="multi" data-vbid="vbid-27694307-mru93chc" data-preset-type-id="FOOTERS" data-preview-style="style-27694307-zbawwujg" data-style="style-f3095-j0xhjgzynh" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="flex"
		data-ARRANGER_COLS="0"
		data-ARRANGER_ITEM_MAX_WIDTH="400"
		data-ARRANGER_ITEM_MIN_WIDTH="230"
		data-ARRANGER_ITEM_RATIO="1"
		data-ARRANGER_ITEM_SPACING="20"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="true"
		data-AUTO_PLAY_DURATION="4"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT="effect-fadein"
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
		
		<div id="items-holder-wrapper">
			<div id="items-holder">
	
	
	
	
		<div  id="vbid-27694307-rr4ycud1"  class="sub item-box  page-box style-27694307-zbawwujg          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-zbawwujg" data-preview-styleid='style-27694307-zbawwujg' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-rr4ycud1" data-preview-style="style-27694307-zbawwujg" data-style="style-f3095-yq39oaomol" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-rr4ycud1" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									<!--  BY SPECIFIC ORDER -->
									
										
											
												
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-vcasybbb" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Laulls&amp;Co Law Group</h2>
	
</div>
<br class="lower-line-break" />

											
										
									
										
											
												<div class="preview-item-links order-handle removable-parent" style="display:inline-block;">

	<span id="vbid-27694307-6cj6rceo"  class="preview-element Link item-link magic-circle-holder text-element custom" data-menu-name="PREVIEW_LINK" >Call us: 1337313373</span>

</div>
											
										
									
										
											
												
<br class="upper-line-break" />
	<div class="preview-social-wrapper removable-parent order-handle"  >
		<div id="vbid-27694307-wh5nsq1q" class="preview-element preview-social-holder magic-circle-holder" data-menu-name="PREVIEW_SOCIAL" data-theme="6" >
			
			<div id="FACEBOOK" class='link-entry ' data-menu-name="PREVIEW_SOCIAL" style='display:inline-block;' data-title="FACEBOOK" data-img-url="//www.imcreator.com/images/socialmedia/6facebook.png">
				<a class='social-link-url' href="http://www.facebook.com/" target='_blank'><img class='preview-link-img nopin ' data-pin-nopin="true" src="//www.imcreator.com/images/socialmedia/6facebook.png" ></a>
			</div>
			
			<div id="TWITTER" class='link-entry ' data-menu-name="PREVIEW_SOCIAL" style='display:inline-block;' data-title="TWITTER" data-img-url="//www.imcreator.com/images/socialmedia/6twitter.png">
				<a class='social-link-url' href="http://www.twitter.com/" target='_blank'><img class='preview-link-img nopin ' data-pin-nopin="true" src="//www.imcreator.com/images/socialmedia/6twitter.png" ></a>
			</div>
			
			<div id="INSTAGRAM" class='link-entry ' data-menu-name="PREVIEW_SOCIAL" style='display:inline-block;' data-title="INSTAGRAM" data-img-url="//www.imcreator.com/images/socialmedia/6instagram.png">
				<a class='social-link-url' href="http://www.instagram.com" target='_blank'><img class='preview-link-img nopin ' data-pin-nopin="true" src="//www.imcreator.com/images/socialmedia/6instagram.png" ></a>
			</div>
			
		</div> 
	</div>
	<br class="lower-line-break" />

											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		
		
		
	
		
			</div>
		</div>
		
		
	
	</div>
</div>
					
			</div>
		</div>
		<!-- FOOTERS END -->
		
		
	
	
	<!-- MAPS START -->
		<div  id="vbid-27694307-uyr8wslp"  class="master item-box  gallery-box style-27694307-3sdvlgcv    button-effects btn_hover3      " data-holder-type="gallery"" data-holder-type="gallery"  data-child-type="STYLE"  data-styleid="style-27694307-3sdvlgcv" data-preview-styleid='style-27694307-3sdvlgcv' data-preset-type-id="MAPS">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="gallery-wrapper item-wrapper maps-wrapper ">
					
					<div  class="sub container style-27694307-3sdvlgcv content flex   " data-itemtype="folder" data-creator="" data-itemname="LOADING Copy"  data-itemslug="loading-copy" data-itemstyleid="style-27694307-3sdvlgcv" data-margintop="" data-arranger="flex" data-layout="multi" data-vbid="vbid-27694307-uyr8wslp" data-preset-type-id="MAPS" data-preview-style="style-27694307-3sdvlgcv" data-style="style-f3095-j0xhjgzynh" data-absolute-path="/free/sdoifjaspdfidsf"  >
	<!-- ARRANGER SETTINGS -->
	<div class="arranger-settings" style="display:none;"
		data-ARRANGER_TYPE="flex"
		data-ARRANGER_COLS="0"
		data-ARRANGER_ITEM_MAX_WIDTH="400"
		data-ARRANGER_ITEM_MIN_WIDTH="230"
		data-ARRANGER_ITEM_RATIO="1"
		data-ARRANGER_ITEM_SPACING="20"
		data-ARRANGER_ITEMS_PER_PAGE="all"
		data-ARRANGER_ORDER_TYPE="regular"
		data-AUTO_PLAY="true"
		data-AUTO_PLAY_DURATION="4"
		data-SLIDE_EFFECT="SLIDE"
		data-FLEX_ELEMENT_EFFECT="effect-fadein"
		data-FLEX_ARROWS="https://lh3.googleusercontent.com/ZMARmveTg1geksYKXZKdh71KW09XrhDLg8N-XrfXCGsDBEHnuKwhmYpHd55Y2-NwuwLX8qsyx26JNyJWtr1jEcxD=s50"
	></div>
	<div class="layout-settings" style="display:none;" data-type="multi"></div>
	<div id="children">
	
		
		<div id="items-holder-wrapper">
			<div id="items-holder">
	
	
	
	
		<div  id="vbid-27694307-4gymajeu"  class="sub item-box  page-box style-27694307-3sdvlgcv          " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-3sdvlgcv" data-preview-styleid='style-27694307-3sdvlgcv' data-preset-type-id="UNRESOLVED">
			
			<div class="page-wrapper item-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-4gymajeu" data-preview-style="style-27694307-3sdvlgcv" data-style="style-f3095-yq39oaomol" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-4gymajeu" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									<!--  BY SPECIFIC ORDER -->
									
										
											
												
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
		<a href="https://www.copyleft.org"  data-link-type="EXTERNAL" target="_blank" >
	
	<h3 id="vbid-27694307-k7fag132" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">All rights are left.</h3>
</a>
</div>
<br class="lower-line-break" />

											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>



	
		
			</div>
		</div>
		
		
	
	</div>
</div>
					
			</div>
		</div>
		<!-- MAPS END -->
		
		
	
	
	<!-- FORM START -->
		<div  id="vbid-27694307-78zsgaix"  class="master item-box  page-box style-27694307-ox1niwyw         stripe_popup_app_hide " data-holder-type="page"" data-holder-type="page"  data-child-type="STYLE"  data-styleid="style-27694307-ox1niwyw" data-preview-styleid='style-27694307-ox1niwyw' data-preset-type-id="FORM">
			<div id="no-image" class="stripe-background load-high-res " ></div>
			<div class="page-wrapper item-wrapper form-wrapper ">
					
					<div class="item-content leaf multi_layout page content -container" data-self="vbid-27694307-78zsgaix" data-preview-style="style-27694307-ox1niwyw" data-style="style-e4cfb-6huavktiba" data-orig-thumb-height="" data-orig-thumb-width="" data-vbid="vbid-27694307-78zsgaix" data-bgimg="">
<div  class="multi-container preview image-cover" >
	<div class="Picture item-preview">
		<div class="preview-image-holder">
			<div id="no-image" class="background-image-div preview-element image-source magic-circle-holder unfold-left load-high-res" data-menu-name="BACKGROUND_IMAGE" style="">
			</div>
			<div class="helper-div middle-center" >
			<!-- <div class="benet" style="min-height:inherit;"></div> -->
			<div class="pic-side">
				<div class="vertical-aligner">
					<div id="no-image-holder"  class="preview-image-holder inner-pic-holder" data-menu-name="PREVIEW_INLINE_IMAGE_HOLDER">

	<div  id="no-image" class="inner-pic preview-element  magic-circle-holder  load-high-res " data-menu-name="PREVIEW_INLINE_IMAGE" style=""  data-orig-width="" data-orig-height="" >
		
		
		
		
		
		
	</div>
	
</div>
	

				</div>
			</div>	
				<div class="text-side shrinker-parent">
					<div class="vertical-aligner">
						<div class="item-details preview-content-wrapper  multi" style="position:relative;">
							<div class="draggable-div-holder"></div>
							<div class="preview-content-holder shrinker-content">
								
									<!--  BY SPECIFIC ORDER -->
									
										
											
												
<br class="upper-line-break" />
<div class="preview-title-holder removable-parent order-handle">
	
	<h2 id="vbid-27694307-elnfajua" class="preview-element preview-title magic-circle-holder inner-page text-element quick-text-style-menu custom  allow-mobile-hide" data-menu-name="PREVIEW_TITLE" >Contact Us Today</h2>
	
</div>
<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-subtitle-holder removable-parent order-handle">
	
	<h3 id="vbid-27694307-bzwsnnh2" class="preview-element preview-subtitle magic-circle-holder text-element quick-text-style-menu   allow-mobile-hide" data-menu-name="PREVIEW_SUBTITLE">we are here to help!</h3>

</div>
<br class="lower-line-break" />






















												
<br class="upper-line-break" />
<div class="preview-form order-handle removable-parent">
	<div class="field-holder">
		
		<input autocomplete="off" type="text"  id="vbid-27694307-4xief0tt"  name="Name" class="preview-element Field  magic-circle-holder field-text" data-menu-name="PREVIEW_FIELD" placeholder="Name" >
		
	</div>
	</div>
	<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-form order-handle removable-parent">
	<div class="field-holder">
		
		<input autocomplete="off" type="text"  id="vbid-27694307-384sdz2q"  name="Email" class="preview-element Field  magic-circle-holder field-email" data-menu-name="PREVIEW_FIELD" placeholder="Email" >
		
	</div>
	</div>
	<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-form order-handle removable-parent">
	<div class="field-holder">
		
		<input autocomplete="off" type="text"  id="vbid-27694307-sma5nhgl"  name="Phone" class="preview-element Field  magic-circle-holder field-phone" data-menu-name="PREVIEW_FIELD" placeholder="Phone" >
		
	</div>
	</div>
	<br class="lower-line-break" />

											
										
									
										
											
												
<br class="upper-line-break" />
<div class="preview-form order-handle removable-parent">
	<div class="field-holder">
		
		<textarea autocomplete="off" type="text"  id="vbid-27694307-srb12kxu"  name="Message" class="preview-element Field  magic-circle-holder field-message" data-menu-name="PREVIEW_FIELD" placeholder="Message" ></textarea>
		
		
	</div>
	</div>
	<br class="lower-line-break" />

											
										
									
										
											
												<div class="preview-item-links order-handle removable-parent" style="display:inline-block;">

		<a class="removable-parent" href=""  data-link-type="SUBMIT"  data-text='' target="_blank" >

	<span id="vbid-27694307-m4dfyzno"  class="preview-element Link item-link magic-circle-holder text-element custom" data-menu-name="PREVIEW_LINK" >Send</span>
</a>
</div>
											
										
									
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>

<div class="layout-settings" style="display:none;" data-type="multi"></div>
					
			</div>
		</div>
		<!-- FORM END -->
        <!-- DEBUG: <?=$GIT_RELEASE_VERSION?> -->

	
	</div>
</div>
				</div>
			</div>
		<div class="light-box-wrapper  space-layer" style="display:none;" tabindex="0">
	<div class="light-box-image-holder">
		<div class="light-box-image animated">
			
		</div>
		<div class="lightbox-text-wrapper ">
			<div class="lightbox-text-holder animated">
				<div class="lightbox-title"></div>
				<div class="lightbox-subtitle"></div>
				<div id="paginator" style="display:none;">
				</div>
			</div>
		</div>
	</div>
	    <img src="https://lh3.googleusercontent.com/EWqW7DEI4kOTRMLjK2-ObFHp-EYBt5apFYZ1LVFAhLtTLjigCRfx5hCCTKbIjIm68VQ00p9twloHJ9w8=s50" class="download-gallery-btn clickable" style="display: none;">
		<img src="https://lh3.googleusercontent.com/TgRyMQvJ3_h9RmOnu7AlhIE7NLOOBsRoBounARrs8fQv8HCRPaFtpBneSqJOSZpI6l7He_bAZKN179JBig=s50" class="close-lightbox-btn clickable" style="opacity: 1;">
		<img src="https://lh3.googleusercontent.com/43-pXHjwrpmVO8Oean-6BD0uzARvcqUQrpdi7Yw2bxaXwEoP21UdN5kW6Ks9pdOxf7ropMUrh0djgYPwYPU=s50" class="lightbox-arrow lightbox-left clickable top-layer" >
		<img src="https://lh3.googleusercontent.com/9rwgVnDglPdPFugSu98fhDmxzjXC9KovZ_7BuHkXPIv6jvg9S96flGnhL_e4y8mIpPpZQstfqEV-WitY=s50" class="lightbox-arrow lightbox-right clickable top-layer" >
</div>

	</div>
	
	<script src="//www.imcreator.com/js/lightbox.js?v=1.4.8d4"  type="text/javascript"></script>
	<script src="//www.imcreator.com/js/spimeengine.js?v=1.4.8d4"></script>


	
</body>

	
</html>

<?php }
