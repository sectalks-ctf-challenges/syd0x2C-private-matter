FROM ubuntu:18.04
RUN DEBIAN_FRONTEND="noninteractive" apt update
RUN DEBIAN_FRONTEND="noninteractive" apt-get update
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y upgrade
RUN DEBIAN_FRONTEND="noninteractive" apt-get update --fix-missing
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install php php-fpm php-common php-cli git
RUN DEBIAN_FRONTEND="noninteractive" apt-get install -y nginx-full
RUN mkdir /tmp/sess
COPY flag.txt /flag.txt
RUN chown root:root /flag.txt
RUN chmod 664 /flag.txt
COPY nginx/nginx.conf /etc/nginx/
COPY nginx/default /etc/nginx/sites-enabled/
RUN rm -fr /var/www/html/*
COPY app/* /var/www/html/
RUN chmod 555 /var/www/html
RUN chmod -R 555 /var/www/html
COPY run.sh /
RUN chmod 770 /tmp/sess
RUN chown www-data:www-data /tmp/sess
WORKDIR /var/www/html
RUN git init && git config user.name "0xBADCA7" && git config user.email "thefuckingcat@microsoft.com" && git add . --all && git commit -am "Merge into master"
RUN chmod -R 555 /var/www/html/.git
RUN chmod +x /run.sh
ENTRYPOINT ["/run.sh"]
